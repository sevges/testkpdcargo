<?php

use yii\db\Migration;

/**
 * Class m180213_072704_init
 */
class m180213_072704_init extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            // имя
            'firstname' => $this->string(),
            // фамилия
            'lastname' => $this->string(),
            // дата рождения
            'birthdate' => $this->date(),
            // пол
            'male' => $this->boolean(),
            // номер телефона
            'phone' => $this->string(20),
        ]);

        $this->createTable('address', [
            'id' => $this->primaryKey(),
            // связь с пользователем
            'user_id' => $this->integer()->notNull(),
            // название
            'name' => $this->string(),
            // адрес
            'address' => $this->string(),
        ]);

        $this->addForeignKey(
            'fk-address-user_id',
            'address',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-address-user_id', 'address');
        $this->dropTable('address');
        $this->dropTable('user');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180213_072704_init cannot be reverted.\n";

        return false;
    }
    */
}
