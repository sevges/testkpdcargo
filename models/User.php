<?php

namespace app\models;

use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;

class User extends ActiveRecord
{
    public static function tableName()
    {
        return 'user';
    }

    public function behaviors()
    {
        return [
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['birthdate'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'birthdate',
                    ActiveRecord::EVENT_AFTER_FIND => 'birthdate',
                ],
                'value' => function ($event) {
                    if ($event->name == 'afterFind') {
                        return date('d.m.Y', strtotime($this->birthdate));
                    } else {
                        return date('Y-m-d', strtotime($this->birthdate));
                    }
                },
            ],
        ];
    }

    public function rules()
    {
        return [
            [
                [
                    'firstname',
                ],
                'required',
                'message' => "Имя должно быть указано",
            ],
            [
                [
                    'birthdate',
                ],
                'date',
                'format' => 'php:d.m.Y',
                'max' => strtotime('now'),
                'message' => "Дата рождения должна быть указана верно",
                'tooBig' => "Дата рождения должна быть указана в прошлом",
            ],
            [
                [
                    'lastname',
                    'male',
                    'phone',
                ],
                'safe',
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'firstname' => 'Имя',
            'lastname' => 'Фамилия',
            'birthdate' => 'Дата рождения',
            'male' => 'Пол',
            'phone' => 'Телефон',
        ];
    }

    public static function getMaleFormat()
    {
        return [
            'Женский',
            'Мужской',
        ];
    }

    public function getMaleLabel()
    {
        return $this->male ? 'Мужской' : 'Женский';
    }
}
