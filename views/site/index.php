<?php

use yii\grid\GridView;
use yii\helpers\Html;
use \app\models\User;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

  <h1><?=Html::encode($this->title)?></h1>

  <p>
      <?=Html::a('Добавить', ['create'], ['class' => 'btn btn-success'])?>
  </p>

    <?=GridView::widget([
        'dataProvider' => $dataProvider,
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'booleanFormat' => User::getMaleFormat(),
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'firstname',
            'lastname',
            'birthdate',
            'male:boolean',
            'phone',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);?>
</div>
