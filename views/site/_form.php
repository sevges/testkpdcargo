<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?=$form->field($model, 'id')->hiddenInput()->label(false)?>

    <?=$form->field($model, 'firstname')->textInput(['maxlength' => true])?>

    <?=$form->field($model, 'lastname')->textInput(['maxlength' => true])?>

    <?=$form->field($model, 'birthdate')->widget(MaskedInput::className(), [
        'clientOptions' => ['alias' => 'dd.mm.yyyy'],
    ])?>

    <?=$form->field($model, 'male')->checkbox(['label' => 'Мужской',
        'uncheck' => '0', 'checked' => '1'])->label('Пол')?>

    <?=$form->field($model, 'phone')->widget(MaskedInput::className(), [
        'mask' => '+9 (999) 999-9999',
    ])?>

  <div class="form-group">
      <?=Html::submitButton('Сохранить', ['class' => 'btn btn-success'])?>
  </div>

    <?php ActiveForm::end(); ?>

</div>
